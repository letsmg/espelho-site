package com.example.luiz.espelho;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView mWebview = new WebView(this);
        mWebview.loadUrl("http://www.viaderegra.com");
        setContentView(mWebview);
    }
}
